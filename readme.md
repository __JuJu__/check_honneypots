# Interaction with FTP, SMB and phpMyAdmin

The goal of this scripts is to interact with an FTP server, SMB server or a PhpMyAdmin administration interface.
In the playbook, I locally run commands from the python script against the different services as per my requirements, then I check the syslog alerts on the syslog server (IP adresses in the hosts file).

## The python script

The usage is :

```
usage: test_honneypots.py [-h] [-H HOST] [-U _U] [-P _P] [-T {ftp,smb,php}]

optional arguments:
  -h, --help            show this help message and exit
  -H HOST, --host HOST  Provide destination host. Defaults to localhost
  -U _U, --user _U      Provide a login to test
  -P _P, --pwd _P       Provide a password to test
  -T {ftp,smb,php}, --type {ftp,smb,php}
                        Provide the type of honey pot service to test
```

## The playbook

The playbook is written in way to benefit from tags. You can choose between ftp, smb, php and all

```
ansible-playbook check_honneypot.yaml -K -t all
```

The configuration of the hosts, syslog, etc are dedicated to my configuration, hence should be modified as per your requirements.