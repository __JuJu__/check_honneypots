#!/usr/bin/python3.9
"""Test the different honey pots FTP SMB PHP
This offers some helper functions to interact with the FTP server, SMB server and PhpMyAdmin service
"""
from ftplib import FTP
#pip install pysmb
from smb.SMBConnection import SMBConnection
from argparse import ArgumentParser
#import argparse
from requests import post
import socket
import logging
import logging.handlers
from mysql.connector import connect, Error
import time

SPLUNK = '192.168.1.30'
SPLUNK_PORT = 514

# Parser definitions
parser = ArgumentParser()
parser.add_argument('-H', '--host',
                    default='localhost',
                    dest='host',
                    help='Provide destination host. Defaults to localhost',
                    type=str
                    )

parser.add_argument('-U', '--user',
                    default='TEST_USER',
                    dest='_u',
                    help='Provide a login to test',
                    type=str
                    )

parser.add_argument('-P', '--pwd',
                    default='TEST_PASSWORD',
                    dest='_p',
                    help='Provide a password to test',
                    type=str
                    )

parser.add_argument('-T','--type',
                    choices=('ftp', 'smb', 'php','sql'),
                    dest='type',
                    default='ftp',
                    help='Provide the type of honey pot service to test'
                    )


args = parser.parse_args()

ADDR=args.host


# Helper function definitions
def test_ftp(ip):
    """Test the FTP login password against the honey pot server"""
    with FTP(ip) as ftp:
        try:
            ret = ftp.login(user=args._u, passwd = args._p)    
        except:
            ret = "FTP exception"
    return ret

def test_samba(ip):
    """Test the SMB login password against the honey pot server"""
    try:
        conn = SMBConnection(args._u, args._p, "UBUNTU", "", use_ntlm_v2 = True)
        ret = conn.connect(ip, 445)
    except:
        ret = ret = "SMB exception"
    return ret

def test_php(ip):
    """Test the phpmyadmin login password against the honey pot server"""
    url = "http://"+ip+"/index.php"
    payload = {'pma_username': args._u, 'pma_password': args._p}
    r = post(url,data=payload)
    return r

def test_sql(ip):
    """Test the sql login password against the honey pot server"""
    try:
        with connect(host = ip,user = args._u,password = args._p) as connection:
            print(connection)
    except Error as e:
        print(e)
    

def log():
    logger = logging.getLogger('ControlNodeLogs')
    logger.setLevel(logging.WARNING)
    syslog = logging.handlers.SysLogHandler(address=(SPLUNK, SPLUNK_PORT),socktype=socket.SOCK_STREAM)
    formatter = logging.Formatter('%(asctime)s SOC: %(levelname)s[%(name)s] %(message)s', datefmt= '%b %d %H:%M:%S')
    syslog.setLevel(logging.WARNING)
    syslog.setFormatter(formatter)
    logger.addHandler(syslog)
    logger.warning("SOC INTERACTION WITH HONEYPOT")
    time.sleep(2)


log()

#Test the input in the command line and according to this run the program on the right target
if args.type == 'ftp':
    test_ftp(ADDR)
elif args.type == 'smb':
    test_samba(ADDR)
elif args.type == 'sql':
    test_sql(ADDR)
else:
    test_php(ADDR)

